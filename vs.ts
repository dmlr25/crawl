import * as puppeteer from 'puppeteer';
import * as fs from 'fs';
import { Data, Listing } from './interfaces'

(async () => {

    // validate input arguments
    if (process.argv.length != 3) {
        console.log("Usage: ts-node vs.ts input.json");
        process.exit(0);
    }

    console.log("ld vs.ts | > out.json");

    // init web-driver
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // read input json data
    try{
    var input = fs.readFileSync(process.argv[2], 'utf8');
    } catch(err){
        console.log("file read error ~ " + process.argv[2]);
        process.exit(0);
    }
    const branch: Data = JSON.parse(input);

    // loop through each data entry
    for (const book of branch.data.books) {

        if (book.ISBN13 != null) {

            // query token for vanschaik.com
            var query = `https://www.vanschaik.com/product/?productstype=product&action=product&productssearchstring=${book.ISBN13}`;
            var query = (query);
            await page.goto(query);

            // grab main price element
            const pContentElement = await page.$('.product-content');

            if (pContentElement) {

                // retreive main product link 
                const trace = await page.evaluate(() => document.querySelectorAll('.product-content a')[0].getAttribute('href'));
                await page.goto(trace);

                // retreive main price data
                const pWholeElement = await page.$('.product_price_text');
                
                var priceValue: string = null;
                if (pWholeElement) {
                    // read price text from element
                    priceValue = await (await pWholeElement.getProperty('textContent')).jsonValue();
                    // strip R
                    priceValue = priceValue.replace('R', '');
                }

                // retreive main edition element
                var edition = await page.$$('[data-th="ISBN Number"]');
                // retreive edition text value
                // edition value may exist in either row 3 or 4
                // check each variant
                var editionValue = await (await edition[3].getProperty('textContent')).jsonValue();
                // check if string contains integer
                var regex = /\d/g
                if (regex.test(editionValue) == true) {
                    // retreive first valid integer
                    editionValue = editionValue.match(/\d+/)[0];
                }
                else {
                    // edition not found in row 3, check 4
                    editionValue = await (await edition[4].getProperty('textContent')).jsonValue();
                    if (regex.test(editionValue) == true) {
                        editionValue = editionValue.match(/\d+/)[0];
                    }
                    else {
                        editionValue = null;
                    }
                }

                if (pWholeElement) {

                    if (priceValue == "Out of Stock") {
                        console.log('-');
                        continue;
                    }
                }

                // define listing json object
                var listing: Listing = {
                    isbn: book.ISBN13,
                    edition: '',
                    source: 'VAN_SCHAIK',
                    price: { zar: '', usd: '' }
                };

                // strip spaces
                listing.price.zar = priceValue.replace(' ', '');
                listing.edition = editionValue;

                // write json entry for listing
                fs.appendFileSync('out.json', JSON.stringify(listing) + ',\n');

                 // output console messages
                console.log("* " + book.ISBN13 + " @vs");
                console.log('  |  |')
                console.log("  |   --- (price)----> " + priceValue.replace(' ', ''));
                console.log("   ------ (edition)--> " + editionValue);
                console.log();
            }
            else {
                // no price found for entry, output isbn
                console.log("* " + book.ISBN13 + " @vs" + "\n-");
            }
        }
    }

    // session close
    await browser.close();
    console.log("end vs.ts");

})();


