import * as puppeteer from 'puppeteer';
import * as fs from 'fs';
import { Data, Listing } from './interfaces'

(async () => {

    // validate input arguments
    if (process.argv.length != 3) {
        console.log("Usage: ts-node tl.ts input.json");
        process.exit(0);
    }

    console.log("ld tl.ts | > out.json");

    // init web-driver
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // read input json data
    try{
    var input = fs.readFileSync(process.argv[2], 'utf8');
    } catch(err){
        console.log("file read error ~ " + process.argv[2]);
        process.exit(0);
    }
    const branch: Data = JSON.parse(input);

    // loop through each data entry
    for (const book of branch.data.books) {

        if (book.ISBN13 != null) {

            // query token for takalot.com
            var query = `https://www.takealot.com/books/all?qsearch=${book.ISBN13}`;
            var query = (query);
            await page.goto(query);

            // retreive HTML price element
            const pWholeElement = await page.$('span.amount');

            if (pWholeElement != null) {

                // retreive price text from element
                var pWhole = await (await pWholeElement.getProperty('textContent')).jsonValue();

                // retreive main product link 
                const href = await page.evaluate(() => document.getElementById('pos_link_0').getAttribute('href'));
                // visit product page for additional information (edition number)
                await page.goto('https://www.takealot.com' + href + '/product-information');

                // multiple html variations exist for edition data
                // check each variant
                var edition = await page.$('.product-info-row-meta_edition');

                if (edition == null) {
                    edition = await page.$('.product-info-row-edition');
                }

                // assuming edition exists, verify edition format
                // with regex

                if (edition != null) {
                    // get edition text from element
                    var editionValue = await (await edition.getProperty('textContent')).jsonValue();
                    // check if string contains integer
                    var regex = /\d/g

                    // edition verified, truncate trailing zeros and strip integer value
                    if (regex.test(editionValue) == true) {
                        editionValue = editionValue.match(/\d+/)[0];
                        editionValue = editionValue.replace(/\b0+/g, '')
                    }
                    else {
                        editionValue = null;
                    }
                }

                // element was null
                if (edition == null) {
                    editionValue = null;
                }

                // define listing json object
                var listing: Listing = {
                    isbn: book.ISBN13,
                    edition: '',
                    source: 'TAKEALOT',
                    price: { zar: '', usd: '' }
                };

                // strip comma's 
                listing.price.zar = pWhole.replace(',', '');
                listing.edition = editionValue;

                // write json entry for listing
                fs.appendFileSync('out.json', JSON.stringify(listing) + ',\n');

                // output console messages
                console.log("* " + book.ISBN13 + " @tl");
                console.log('  |  |')
                console.log("  |   --- (price)----> " + pWhole.replace(',', ''));
                console.log("   ------ (edition)--> " + editionValue);
                console.log();
            }
            else {
                // no price found for entry, output isbn
                console.log("* " + book.ISBN13 + " @tl" + "\n-");

            }
        }
    }

    // session close
    await browser.close();
    console.log("end tl.ts");

})();


