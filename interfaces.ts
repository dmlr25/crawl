// define json interfaces for book data
export interface Entity{
    ISBN13 : string;
};
export interface Books{
    books: Entity[];
}
export interface Data{
    data: Books;
}
export interface Price{
    usd: string;
    zar: string;
}
export interface Listing{
    isbn: string;
    edition: string;
    source: string;
    price: Price;
}