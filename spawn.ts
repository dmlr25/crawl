import * as cp from 'child_process';
import * as ns from 'node-schedule';

// setup scheduler
var date = new Date();
var rule = new ns.RecurrenceRule();
rule.dayOfWeek = date.getDay()-1;

start();
var job = ns.scheduleJob(rule, ()=>{
start();
})


// init scripts
function start(){
// start child procs
const vs = cp.spawn('ts-node',[ 'vs.ts', 'books.json']);
const lt = cp.spawn('ts-node',[ 'tl.ts', 'books.json']);

// redirect io (mixed io)
// channel 1
vs.stdout.on('data', (data)=>{
    process.stdout.write( data.toString());
    
});
// channel 2
lt.stdout.on('data', (data)=>{
    process.stdout.write( data.toString());
});
};
   
    


